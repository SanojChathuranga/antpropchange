package test;

import java.text.SimpleDateFormat;
import java.util.*;

public class TestOther {

	public static void main(String[] args) {
		
		Calendar cMin = Calendar.getInstance();
		SimpleDateFormat sdf  = new SimpleDateFormat("dd/MM/yyyy");
		Calendar cMax = Calendar.getInstance();
		cMax.add(Calendar.MONTH, +1);
		 
		long minimum = cMin.getTimeInMillis();
		long maximum = cMax.getTimeInMillis();
		Calendar cRand = Calendar.getInstance();
		String f = sdf.format( cRand.getTime());
		System.out.println("f : "+f);
		
		Random rand = new Random();
		int  n = rand.nextInt(25) + 1;
		System.out.println("n : "+n);
		 
		for(int i = 0 ; i < 50 ; i++)
		{ 
			long randomNum = minimum + (long)(Math.random()*maximum);
			//Calendar cRand = Calendar.getInstance();
			System.out.println(i);
			cRand.setTimeInMillis(randomNum);
			if( cMin.before(cRand) ){
				//(sdf.format( cRand.getTime()));
				System.out.println();
				System.out.println("Min : "+sdf.format( cMin.getTime()));
				System.out.println("Max : "+sdf.format( cRand.getTime()));
				System.out.println("===========================");
				break;
			}	
		}
	}
}
