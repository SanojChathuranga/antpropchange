package test;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

//import system.supportParents.ReadProperties;
//import system.supportParents.SupportMethods;


public class test {
	
	HashMap<String, String> propertyMap = new HashMap<String, String>();
	String path = "Property.properties";
	
	
	@Before
	public void born()
	{
		try {
			propertyMap = readpropeties(path);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void live()
	{	
		System.out.println("Name : "+propertyMap.get("Name"));
		System.out.println("Age : "+propertyMap.get("Age"));
		System.out.println("School : "+propertyMap.get("School"));
		System.out.println("MartialStatus : "+propertyMap.get("MartialStatus"));
		System.out.println("Crazy : "+propertyMap.get("Crazy"));
	}
	
	@After
	public void die()
	{
		
	}
	
    public static HashMap<String, String> readpropeties(String path) throws IOException {
		
    	FileInputStream fs = null;
    	Properties prop = new Properties();
		HashMap<String, String>    mymap = null ;
		
		try {
			fs= new FileInputStream(new File(path));
			prop.load(fs);
			mymap= new HashMap<String, String>((Map)prop);
			return mymap;
		
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return mymap;

	}
}
